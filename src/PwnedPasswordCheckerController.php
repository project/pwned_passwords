<?php

namespace Drupal\pwned_passwords;

use Esolitos\PwnedPasswords\PwnageValidator;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Class PwnedPasswordCheckerController.
 */
class PwnedPasswordCheckerController implements PwnedPasswordCheckerControllerInterface {

  /**
   * @var \Esolitos\PwnedPasswords\PwnageValidator
   */
  protected $validator;

  public function __construct() {
    $this->validator = new PwnageValidator();
  }

  /**
   * {@inheritDoc}
   */
  public function isPasswordPwned(string $plaintext_password, int $pwned_threshold = 0): ?bool {
    $pwn_count = $this->getPasswordPwnage($plaintext_password);
    // Null value indicates an error, forward this info to the caller.
    if (is_null($pwn_count)) {
      return NULL;
    }

    return $pwned_threshold <= $pwn_count;
  }

  /**
   * {@inheritDoc}
   */
  public function getPasswordPwnage(string $plaintext_password): ?int {
    $drupal_static_fast['pwned'] = &drupal_static(__CLASS__ . __METHOD__);

    // If we have not checked the password al;ready fetch the info from the remote service.
    if (!isset($drupal_static_fast['pwned'][$plaintext_password])) {
      try {
        $drupal_static_fast['pwned'][$plaintext_password] = $this->validator
          ->getPasswordPwnage($plaintext_password);
      }
      catch (GuzzleException $exception) {
        watchdog_exception('pwned_passwords', $exception);
        return NULL;
      }
    }

    return $drupal_static_fast['pwned'][$plaintext_password];
  }


}
