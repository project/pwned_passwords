<?php

namespace Drupal\pwned_passwords;

/**
 * Interface PwnedPasswordCheckerControllerInterface.
 */
interface PwnedPasswordCheckerControllerInterface {

  /**
   * Check whether a password ever appears in the HIBP database.
   *
   * @param string $plaintext_password
   *   The password to check, in plain text.
   * @param int $pwned_threshold
   *   Minimum amount of occurrences to consider a password "Pwned"
   *
   * @return bool|null
   *   Indicates if a password is pwned, or null if it was not possible to check the status.
   */
  public function isPasswordPwned(string $plaintext_password, int $pwned_threshold = 0): ?bool;

  /**
   * Checks the number of times a password is present in the HIBP database.
   *
   * @param string $plaintext_password
   *   The password to check, in plain text.
   *
   * @return int|null
   *   The number of occurrences, or NULL in case of issues.
   */
  public function getPasswordPwnage(string $plaintext_password): ?int;
}
